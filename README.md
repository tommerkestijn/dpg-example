# AD abonnementen example

This example project for DPG is build with Nuxt3, Vue3 and Tailwind.
Content is loaded with the @nuxt/content libary. 
Components are build as SFCs in combination with composition API and with the setup tag (https://vuejs.org/api/sfc-script-setup.html).


## Demo
You can find a demo of the project here:
https://dpg-example.vercel.app/


## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
# yarn
yarn dev

# npm
npm run dev
```

## Tailwind viewer

http://localhost:3000/_tailwind/ 

## Production

Build the application for production:

```bash
# yarn
yarn build

# npm
npm run build
```

Locally preview production build:

```bash
# yarn
yarn preview

# npm
npm run preview
```

