import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  app: {
    head: {
      title: 'AD abbonementen'
    }
  },

  modules: ['@nuxtjs/tailwindcss', '@nuxt/content'],

  build: {
    postcss: {
      postcssOptions: {
        plugins: {
          tailwindcss: {},
          autoprefixer: {}
        }
      }
    }
  },

  css: [
    '~/assets/css/tailwind.css'
  ]
})
