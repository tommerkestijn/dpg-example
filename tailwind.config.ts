import { Config } from 'tailwindcss'

export default <Config> {
  content: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}'
  ],
  theme: {
    extend: {
      colors: {
        primary: {
          100: '#FCDCCC',
          200: '#FAB19B',
          300: '#F17B67',
          400: '#E34A41',
          500: '#d10a10',
          600: '#B3071B',
          700: '#960523',
          800: '#790325',
          900: '#640127'
        }
      }
    }
  },
  plugins: []
}
